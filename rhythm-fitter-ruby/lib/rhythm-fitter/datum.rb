module RhythmFitter
	class Datum
	  attr_reader :ref
	  attr_accessor :best_measure
	
	  def initialize(ref)
	    @ref = ref
	    @best_measure = nil
	  end
	
	  def measure(m)
	    val = rms(m.to_f)
	    if !self.best_measure || self.best_measure.rms > val
	      m.rms = val
	      self.best_measure = m
	    end
	    val
	  end
	
	  def to_s
	    "\t%6.2f --> %d/%d @ %.1f mm (rms: %.3f)\n" % [ self.ref, self.best_measure.num, self.best_measure.div, self.best_measure.metro, self.best_measure.rms ]
	  end
	
	private
	
		def rms(b)
	    Math.sqrt((self.ref-b)**2) 
		end
	
	end
end
