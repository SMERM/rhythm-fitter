require 'yaml'

module RhythmFitter
	class Manager
	  attr_reader :data, :voices
	
	  def initialize(data)
	    @data = YAML.load(File.open(data, 'r'))
	    @voices = @data.map { |k, v| Voice.new(k, v) }
	    scan
	  end
	
	  def to_s
	    self.voices.each { |v| v.to_s }
	  end
	
	private
	
	  def scan
	    self.voices.each { |v| v.scan }
	  end
	
	end
end
