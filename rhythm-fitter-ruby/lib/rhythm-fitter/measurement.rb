module RhythmFitter

	class Measurement
	  attr_reader :num, :div, :metro
	  attr_accessor :rms
	
	  def initialize(num, div, metro)
	    @num = num
	    @div = div
	    @metro = metro
	    @cache = nil
	  end
	
	  def to_f
	    unless @cache
	      mp = 60.0/self.metro.to_f
	      @cache = (self.num.to_f/self.div.to_f)*mp
	    end
	    @cache
	  end
	
	end

end
